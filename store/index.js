export const state = () => ({
    chat: false
  })
  
  export const getters = {
    getChatState(state) {
      return state.chat
    }
  }
  
  export const mutations = {
    openChat(state) {
      state.chat = true
    },
    closeChat(state) {
        state.chat = false
    }
  }